module gitlab.com/colourdelete/go-aibou-server

go 1.16

// +heroku goVersion 1.16

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/PullRequestInc/go-gpt3 v1.1.2
	github.com/TwinProduction/go-away v1.1.2
	github.com/appleboy/gin-jwt/v2 v2.6.4
	github.com/colourdelete/logflag v1.0.2-0.20210316184925-e03b736a4d51 // indirect
	github.com/d5/tengo/v2 v2.7.0 // indirect
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/jessevdk/go-flags v1.5.0
	github.com/jmoiron/sqlx v1.3.1
	github.com/lib/pq v1.2.0
	github.com/pelletier/go-toml v1.8.1
	github.com/piaohao/godis v0.0.18
	github.com/sirupsen/logrus v1.8.0
	github.com/traefik/yaegi v0.9.15
	github.com/vektah/gqlparser/v2 v2.1.0
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/ratelimit v0.1.0
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898
)

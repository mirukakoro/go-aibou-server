package uid

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"math/big"

	"golang.org/x/crypto/sha3"
)

type ID string

var limit = big.NewInt(9223372036854775807)

//func init() {
//	var ok bool
//	limit, ok = limit.SetString("", 10)
//	if !ok {
//		panic("setting limit for ID failed")
//	}
//}

func New() ID {
	n, err := rand.Int(rand.Reader, limit)
	if err != nil {
		panic(err)
	}
	return ID(fmt.Sprintf("%x", sha3.Sum512([]byte(n.String()))))
}

func Validate(id ID) error {
	b, err := hex.DecodeString(string(id))
	if err != nil {
		return err
	}
	if len(b) != 64 {
		return fmt.Errorf("invalid length: expected %d, got %d", 64, len(b))
	}
	return nil
}

func FromString(id string) (ID, error) {
	if err := Validate(ID(id)); err != nil {
		return "", err
	}
	return ID(id), nil
}

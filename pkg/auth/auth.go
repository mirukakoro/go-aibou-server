package auth

import (
	"crypto/rand"
	"encoding/json"
	"errors"
	"io"

	"gitlab.com/colourdelete/go-aibou-server/pkg/db"
	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"

	"golang.org/x/crypto/sha3"

	"golang.org/x/crypto/pbkdf2"

	"gitlab.com/colourdelete/go-aibou-server/pkg/types"

	jwt "github.com/appleboy/gin-jwt/v2"

	"github.com/gin-gonic/gin"
)

var PBKDF2DefaultIter = 8192
var PBKDF2DefaultKeyLen = 20
var PBKDF2Hash = sha3.New512
var PBKDF2DefaultSaltLen = 64

func OK(c *gin.Context) error {
	return nil
}

func User(c *gin.Context) (types.User, error) {
	claims := jwt.ExtractClaims(c)
	user, ok := c.Get(IDKey)
	if !ok {
		return types.User{}, errors.New("no identifier in request")
	}
	_, ok = claims[IDKey]
	if !ok {
		return types.User{}, errors.New("no identifier in claims")
	}
	jwtUser := user.(*JWTUser)
	return db.BackendToUse.User(uid.ID(jwtUser.ID))
}

type AuthBackend interface {
	CheckID(string) bool
	CheckIDPBKDF2(string, string) bool
	Close() error
}

type PBKDF2Config struct {
	Pass   []byte `json:"pass"`
	Salt   []byte `json:"salt"`
	Iter   int    `json:"iter"`
	KeyLen int    `json:"key_len"`
}

func FromJSON(src []byte) (config PBKDF2Config, err error) {
	err = json.Unmarshal(src, &config)
	return
}

func (config PBKDF2Config) ToJSON() (src []byte) {
	var err error
	if config.Iter == 0 {
		config.Iter = PBKDF2DefaultIter
	}
	if config.KeyLen == 0 {
		config.KeyLen = PBKDF2DefaultKeyLen
	}
	src, err = json.Marshal(config)
	if err != nil {
		panic(err)
	}
	return
}

func (config PBKDF2Config) Key() []byte {
	return pbkdf2.Key(config.Pass, config.Salt, config.Iter, config.KeyLen, PBKDF2Hash)
}

func Salt(saltLen int) (salt []byte) {
	salt = make([]byte, saltLen)
	_, err := io.ReadFull(rand.Reader, salt)
	if err != nil {
		panic(err)
	}
	return salt
}

func SaltDefault() (salt []byte) {
	salt = Salt(PBKDF2DefaultSaltLen)
	return
}

package auth

import (
	"bytes"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"

	"github.com/piaohao/godis"
)

var _ AuthBackend = &Redis{}

type Redis struct {
	opt     godis.Option
	client  *godis.Redis
	getFunc func(string) (string, error)
}

func NewRedis(opt godis.Option) *Redis {
	client := godis.NewRedis(&opt)
	if gin.Mode() == gin.DebugMode {
		logrus.Warn("not ready for production")
		_, err := client.Set(
			"id-94e89bd380edb3493e982813bf943696c49b68021410d7ac0ac0b5ebc8fb3e033a063e94760afe76ed933c17e2e00287a781c7887115ef927634bfd412299b2c",
			string(PBKDF2Config{
				Pass:   []byte("password"),
				Salt:   Salt(PBKDF2DefaultSaltLen),
				Iter:   PBKDF2DefaultIter,
				KeyLen: PBKDF2DefaultKeyLen,
			}.ToJSON()),
		)
		if err != nil {
			logrus.Fatal(err)
		}
	}
	return &Redis{
		opt:    opt,
		client: client,
		getFunc: func(s string) (string, error) {
			return client.Get(fmt.Sprintf("id-%s", s))
		},
	}
}

func (r *Redis) Close() error {
	return r.client.Close()
}

func (r *Redis) CheckID(id string) bool {
	val, err := r.getFunc(id)
	return err == nil && val != ""
}

func (r *Redis) CheckIDPBKDF2(id string, pass string) bool {
	if !r.CheckID(id) {
		return false
	}
	val, err := r.getFunc(id)
	if err != nil || val == "" {
		logrus.Warnf("ignored invalid: %s", err)
		return false
	}
	correct, err := FromJSON([]byte(val))
	if err != nil {
		logrus.Warnf("ignored invalid redis content: %s, %s", err, val)
		return false
	}
	proposed := PBKDF2Config{
		Pass:   []byte(pass),
		Salt:   correct.Salt,
		Iter:   correct.Iter,
		KeyLen: correct.KeyLen,
	}
	return bytes.Equal(correct.Key(), proposed.Key())
}

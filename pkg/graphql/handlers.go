package graphql

import (
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
	"gitlab.com/colourdelete/go-aibou-server/pkg/data"
)

const QueryEndpoint = "/graphql/query"

func QueryHandler() gin.HandlerFunc {
	h := handler.NewDefaultServer(NewExecutableSchema(Config{
		Resolvers:  Root{},
		Directives: DirectiveRoot{},
		Complexity: ComplexityRoot{},
	}))

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

const PlaygroundEndpoint = "/graphql/playground"

func PlaygroundHandler() gin.HandlerFunc {
	h := playground.Handler(data.Text, PlaygroundEndpoint)

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

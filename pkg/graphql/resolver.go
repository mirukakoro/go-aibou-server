package graphql

import (
	"context"
	"errors"
	"os"
	"sync"
	"time"

	"gitlab.com/colourdelete/go-aibou-server/pkg/db"

	"github.com/sirupsen/logrus"

	"gitlab.com/colourdelete/go-aibou-server/pkg/auth"

	"golang.org/x/xerrors"

	"gitlab.com/colourdelete/go-aibou-server/pkg/types"
	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"

	"github.com/gin-gonic/gin"
	"gitlab.com/colourdelete/go-aibou-server/pkg/data"
)

//go:generate go get github.com/99designs/gqlgen
//go:generate go run github.com/99designs/gqlgen

var Engine *types.Engine

type Root struct{}

var _ ResolverRoot = Root{}

type gpt3Resolver struct{}

func (G gpt3Resolver) Temperature(_ context.Context, obj *types.GPT3) (float64, error) {
	return float64(obj.Temperature), nil
}

func (G gpt3Resolver) TopP(_ context.Context, obj *types.GPT3) (float64, error) {
	return float64(obj.TopP), nil
}

func (G gpt3Resolver) FrequencyPenalty(_ context.Context, obj *types.GPT3) (float64, error) {
	return float64(obj.FrequencyPenalty), nil
}

func (G gpt3Resolver) PresencePenalty(_ context.Context, obj *types.GPT3) (float64, error) {
	return float64(obj.PresencePenalty), nil
}

func (r Root) GPT3() GPT3Resolver {
	return gpt3Resolver{}
}

func (r Root) Mutation() MutationResolver {
	return &Mutation{}
}

func (r Root) Query() QueryResolver {
	return &Query{}
}

type Query struct{}

func (q Query) Scenario(ctx context.Context, ids []uid.ID) ([]*Scenario, error) {
	gc, err := GinContextFromContext(ctx)
	if err != nil {
		return nil, err
	}
	user, err := auth.User(gc)
	if err != nil {
		return nil, err
	}
	re := make([]*Scenario, 0)
	for key, scenario := range types.Scenarios {
		if !user.Check(uid.ID(key)) {
			continue
		}
		dest := &Scenario{
			ID: uid.ID(key),
			Meta: &ScenarioMeta{
				Title:       scenario.Meta.Title,
				Description: scenario.Meta.Description,
				Duration:    scenario.Meta.Duration.String(),
				Level:       scenario.Meta.Level,
			},
		}
		dest.Info = make([]*InfoPair, 0)
		for key, val := range scenario.Info {
			dest.Info = append(dest.Info, &InfoPair{
				Key:   key,
				Value: val,
			})
		}
		if scenario.Msgs == nil {
			scenario.Msgs = &types.Msgs{}
		}
		dest.Messages = make([]*types.Msg, len(*scenario.Msgs))
		for _, value := range *scenario.Msgs {
			dest.Messages = append(dest.Messages, &value)
		}
		dest.Characters = make([]*Character, len(scenario.Chars))
		for id, value := range scenario.Chars {
			c := &Character{
				ID:           id,
				Name:         value.Name,
				AddNameToGpt: false,
				Type:         value.Type,
			}
			dest.Characters = append(dest.Characters, c)
		}

		dest.Gpt3 = &scenario.GPT3
		contains := false
		for idx, value := range ids {
			if value == dest.ID {
				contains = true
				ids = append(ids[idx:], ids[:idx]...) // remove ID since it will no longer be needed
				break
			}
		}
		if len(ids) == 0 || contains {
			re = append(re, dest)
		}
	}
	return re, nil
}

func NewServerMeta() ServerMeta {
	return ServerMeta{
		GinMode:    gin.Mode(),
		GinVersion: gin.Version,
		Name:       data.Name,
		Authors:    data.Authors,
	}
}

func (q Query) Meta(_ context.Context) (*ServerMeta, error) {
	meta := NewServerMeta()
	if data.Hash != "" {
		meta.Hash = &data.Hash
	}
	if data.Version != "" {
		meta.Version = &data.Version
	}
	return &meta, nil
}

func (q Query) Conversations(ctx context.Context, ids []uid.ID) ([]*Conversation, error) {
	gc, err := GinContextFromContext(ctx)
	if err != nil {
		return nil, err
	}

	idsMap := map[uid.ID]struct{}{}
	for _, id := range ids {
		idsMap[id] = struct{}{}
	}

	user, err := auth.User(gc)
	if err != nil {
		return nil, err
	}
	re := make([]*Conversation, 0)
	for id, cont := range Engine.Conts {
		if ids != nil {
			if _, ok := idsMap[id]; !ok {
				continue
			}
		}
		cont.Lock.RLock()

		if cont.Meta.User != user.Username {
			logrus.Warn("possible security breach")
			//continue TODO: fix bug where users cant access what they should be able to
		}

		var userUID, gpt3UID uid.ID
		for id, char := range cont.Chars {
			switch char.Type {
			case types.CharacterTypeUser:
				userUID = id
			case types.CharacterTypeGpt3:
				gpt3UID = id
			}
		}

		messages := make([]*types.Msg, len(*cont.Msgs))
		for i, msg := range *cont.Msgs {
			for _, to := range msg.VisibleTo {
				if to == gpt3UID {
					msg.Include = true
				}
			}
			for _, to := range msg.VisibleTo {
				if to == userUID {
					msg.Visible = true
				}
			}
			for _, to := range msg.EditableTo {
				if to == userUID {
					msg.Editable = true
				}
			}
			messages[i] = &types.Msg{
				ID:         msg.ID,
				From:       msg.From,
				Content:    msg.Content,
				VisibleTo:  msg.VisibleTo,
				EditableTo: msg.EditableTo,
				Visible:    msg.Visible,
				Editable:   msg.Editable,
				Include:    msg.Include,
				Rating:     msg.Rating,
			}
		}

		chars := []*Character{}
		for id, char := range cont.Chars {
			chars = append(chars, &Character{
				ID:           id,
				Name:         char.Name,
				Type:         char.Type,
				AddNameToGpt: char.NameVisible,
			})
		}

		c := &Conversation{
			ID: cont.Meta.ID,
			Meta: &ConversationMeta{
				Started: cont.Meta.Started,
				User:    cont.Meta.User,
				Active:  cont.Meta.Active,
				Engine:  cont.Meta.Engine,
				Timeout: cont.Meta.Timeout.String(),
			},
			Messages:   messages,
			Actions:    nil,
			Characters: chars,
			Status:     &cont.State,
		}
		cont.Lock.RUnlock()
		re = append(re, c)
	}
	return re, nil
}

var _ QueryResolver = Query{}

type Mutation struct{}

func (m Mutation) Delete(_ context.Context, conversationID uid.ID, messageID *uid.ID) (bool, error) {
	var ok bool
	func() {
		Engine.Lock.RLock()
		defer Engine.Lock.RUnlock()
		_, ok = Engine.Conts[conversationID]
	}()
	if !ok {
		return false, nil
	}

	Engine.Lock.Lock()
	defer Engine.Lock.Unlock()
	if messageID != nil {
		// TODO: optimize!
		idx := -1
		msgs := Engine.Conts[conversationID].Msgs
		for j, msg := range *msgs {
			if msg.ID == *messageID {
				idx = j
			}
		}
		if idx == -1 {
			return false, errors.New("conversation not found by ID")
		}
		*msgs = append((*msgs)[idx:], (*msgs)[:idx]...)
	} else {
		delete(Engine.Conts, conversationID)
	}
	return true, nil
}

func (m Mutation) TriggerAction(_ context.Context, conversationID uid.ID, actionID uid.ID) (bool, error) {
	conversation, ok := types.Conversations[conversationID]
	if !ok {
		return false, errors.New("conversation not found")
	}
	err := types.RunConv(actionID, conversation, "api")
	if err := db.BackendToUse.Log(logrus.Entry{
		Data: logrus.Fields{
			"action":          "triggerAction",
			"conversation":    conversation,
			"conversation_id": conversationID,
			"action_id":       actionID,
		},
		Time:    time.Now(),
		Level:   logrus.TraceLevel,
		Message: "new message",
	}); err != nil {
		return false, err
	}
	return err == nil, err
}

func (m Mutation) NewMessage(_ context.Context, conversationID uid.ID, message MessageInput) (bool, error) {
	if len(message.Content) > 40 {
		return false, errors.New("message limit exceeded")
	}
	Engine.Lock.Lock()
	conversation, ok := Engine.Conts[conversationID]
	if !ok {
		Engine.Lock.Unlock()
		return false, errors.New("conversation not found")
	}
	conversation.Lock.Lock()
	defer conversation.Lock.Unlock()
	Engine.Lock.Unlock()
	level, err := conversation.ContentFilter(message.Content)
	if err != nil {
		return false, err
	}
	if level != 0 {
		return false, errors.New("unsafe or sensitive")
	}

	messageID := uid.New()
	*conversation.Msgs = append(*conversation.Msgs, types.Msg{
		ID:         messageID,
		From:       message.From,
		Content:    message.Content,
		VisibleTo:  message.VisibleTo,
		EditableTo: message.EditableTo,
	})

	if err := db.BackendToUse.Log(logrus.Entry{
		Data: logrus.Fields{
			"action":          "newMessage",
			"conversation":    conversation,
			"conversation_id": conversationID,
			"message_id":      messageID,
		},
		Time:    time.Now(),
		Level:   logrus.TraceLevel,
		Message: "new message",
	}); err != nil {
		return false, err
	}
	if err := conversation.Refresh(); err != nil {
		return false, err
	}
	return true, nil
}

func (m Mutation) EditMessage(_ context.Context, conversationID uid.ID, messageID uid.ID, message MessageEditInput) (bool, error) {
	Engine.Lock.Lock()
	conversation, ok := Engine.Conts[conversationID]
	if !ok {
		Engine.Lock.Unlock()
		return false, errors.New("conversation not found")
	}
	conversation.Lock.Lock()
	defer conversation.Lock.Unlock()
	Engine.Lock.Unlock()
	msgs := conversation.Msgs
	idx := -1
	for i, msg := range *msgs {
		if msg.ID == messageID {
			idx = i
		}
	}
	if idx == -1 {
		return false, errors.New("conversation not found by ID")
	}
	if message.Content != nil {
		(*msgs)[idx].Content = *message.Content
	}
	if message.VisibleTo != nil {
		(*msgs)[idx].VisibleTo = message.VisibleTo
	}
	if message.EditableTo != nil {
		(*msgs)[idx].EditableTo = message.EditableTo
	}
	if err := db.BackendToUse.Log(logrus.Entry{
		Data: logrus.Fields{
			"action":          "editMessage",
			"conversation":    conversation,
			"conversation_id": conversationID,
			"message_id":      messageID,
		},
		Time:    time.Now(),
		Level:   logrus.TraceLevel,
		Message: "new message",
	}); err != nil {
		return false, err
	}
	return true, nil
}

func (m Mutation) NewConversation(ctx context.Context, scenarioID uid.ID) (*uid.ID, error) {
	gc, err := GinContextFromContext(ctx)
	if err != nil {
		return nil, err
	}

	if user, err := auth.User(gc); err != nil {
		return nil, err
	} else {
		if !user.Check(scenarioID) {
			return nil, errors.New("disallowed")
		}
	}

	var ok bool
	var scenario types.Scenario
	if scenario, ok = types.Scenarios[string(scenarioID)]; !ok {
		return nil, xerrors.New("scenario not found")
	}
	id := uid.New()
	scenario.GPT3.APIKey = os.Getenv("GO_AIBOU_SERVER_OPENAI_API_KEY")
	msgs := *scenario.Msgs
	cont := &types.ConversationContainer{
		Conversation: types.Conversation{
			Meta: types.NewConvMeta(
				scenario.GPT3.Engine,
				5*time.Second, // TODO: add configurable timeout
				id,
			),
			Msgs:     &msgs,
			Scenario: scenario,
			Actions:  map[uid.ID]types.Action{},
			Chars:    scenario.Chars,
			State:    "",
		},
		Access: nil,
		Lock:   sync.RWMutex{},
		Err:    nil,
	}

	Engine.Lock.Lock()
	defer Engine.Lock.Unlock()
	if _, ok := Engine.Conts[id]; ok {
		Engine.Lock.RUnlock()
		return nil, xerrors.New("ID collision!")
	}
	Engine.Conts[id] = cont
	if err := db.BackendToUse.Log(logrus.Entry{
		Data: logrus.Fields{
			"action":          "newConversation",
			"conversation_id": id,
		},
		Time:    time.Now(),
		Level:   logrus.TraceLevel,
		Message: "new message",
	}); err != nil {
		return nil, err
	}
	return &id, nil
}

var _ MutationResolver = &Mutation{}

package graphql

import (
	"context"
	"fmt"

	"github.com/gin-gonic/gin"
)

const GinContextKey = "GinContextKey"

func GinContextToContextMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := context.WithValue(c.Request.Context(), GinContextKey, c)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}

type GetFailError struct{}

func (g GetFailError) Error() string {
	return "could not retrieve *gin.Context"
}

func IsGetFailError(err error) bool {
	_, ok := err.(GetFailError)
	return ok
}

type TypeError struct{}

func (t TypeError) Error() string {
	return "*gin.Context has wrong type"
}

func IsTypeError(err error) bool {
	_, ok := err.(TypeError)
	return ok
}

func GinContextFromContext(ctx context.Context) (*gin.Context, error) {
	ginContext := ctx.Value(GinContextKey)
	if ginContext == nil {
		err := fmt.Errorf("could not retrieve gin.Context")
		return nil, err
	}

	gc, ok := ginContext.(*gin.Context)
	if !ok {
		err := fmt.Errorf("gin.Context has wrong type")
		return nil, err
	}
	return gc, nil
}

package db

import (
	"encoding/json"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"gitlab.com/colourdelete/go-aibou-server/pkg/types"
	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"
)

type SQL struct {
	db *sqlx.DB
}

var _ Backend = SQL{}

const schema = `
CREATE TABLE IF NOT EXISTS log (
	json text
)
`

func NewSQL(driverName string, dataSourceName string) (SQL, error) {
	db, err := sqlx.Connect(driverName, dataSourceName)
	if err != nil {
		return SQL{}, err
	}
	_, err = db.Exec(schema)
	if err != nil {
		return SQL{}, err
	}
	return SQL{
		db: db,
	}, nil
}

func (S SQL) Log(entry logrus.Entry) error {
	tx, err := S.db.Begin()
	if err != nil {
		return err
	}
	src, err := json.Marshal(entry)
	if err != nil {
		return err
	}
	_, err = tx.Exec("INSERT INTO log (json) VALUES ($1)", string(src))
	if err != nil {
		return err
	}
	return tx.Commit()
}

func (S SQL) User(id uid.ID) (types.User, error) {
	return types.User{}, nil
}

func (S SQL) Close() error {
	return S.db.Close()
}

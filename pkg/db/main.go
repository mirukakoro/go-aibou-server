package db

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/colourdelete/go-aibou-server/pkg/types"
	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"
)

var BackendToUse Backend

type Backend interface {
	User(uid.ID) (types.User, error)
	Log(logrus.Entry) error
	Close() error
}

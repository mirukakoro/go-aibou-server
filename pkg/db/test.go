package db

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/colourdelete/go-aibou-server/pkg/types"
	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"
)

type TestNoSecurity struct{}

func (t TestNoSecurity) Log(entry logrus.Entry) error {
	return nil
}

func (t TestNoSecurity) User(id uid.ID) (types.User, error) {
	return types.User{
		Username: "test",
		Allow:    nil,
		Block:    nil,
	}, nil
}

func (t TestNoSecurity) Close() error {
	return nil
}

var _ Backend = TestNoSecurity{}

package types

import "sync"

type ConversationContainer struct {
	Conversation
	Access []string
	Lock   sync.RWMutex
	Err    error
}

func NewConvCont(c Conversation) *ConversationContainer {
	return &ConversationContainer{
		Conversation: c,
	}
}

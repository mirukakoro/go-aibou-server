package types

import (
	"encoding/json"
	"time"
)

type Resp struct {
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

var _ json.Marshaler = Resp{}

func (r Resp) MarshalJSON() ([]byte, error) {
	return json.Marshal(r.Map())
}

func (r Resp) Map() map[string]interface{} {
	return map[string]interface{}{
		"msg":  r.Msg,
		"data": r.Data,
		"time": time.Now().Format(time.RFC3339Nano),
	}
}

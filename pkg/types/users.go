package types

import (
	"encoding/json"

	"gitlab.com/colourdelete/go-aibou-server/pkg/uid"
)

type User struct {
	Username uid.ID              `json:"username"`
	Allow    map[uid.ID]struct{} `json:"allow"` // if both Allow and Block contain elements, then Allow is prioritized
	Block    map[uid.ID]struct{} `json:"block"`
}

func (u User) Check(id uid.ID) bool {
	if u.Allow == nil {
		if u.Block == nil {
			return true
		}
		_, ok := u.Block[id]
		return !ok
	}
	_, ok := u.Allow[id]
	return ok
}

func (u User) ToJSON() (src []byte) {
	src, err := json.Marshal(u)
	if err != nil {
		panic(err)
	}
	return
}

func FromJSON(src []byte) (u User) {
	err := json.Unmarshal(src, &u)
	if err != nil {
		panic(err)
	}
	return
}

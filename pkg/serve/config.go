package serve

import (
	"time"

	"github.com/sirupsen/logrus"
)

type Config struct {
	Interface struct {
		Release bool `toml:"release"`
		Log     struct {
			ReportCaller  bool                  `toml:"report_caller"`
			JSONFormatter *logrus.JSONFormatter `toml:"json_formatter"`
			TextFormatter *logrus.TextFormatter `toml:"text_formatter"`
		} `toml:"log"`
	} `toml:"interface"`
	Ratelimit struct {
		RPS int `toml:"rps"`
	} `toml:"ratelimit"`
	DB struct {
		Redis *struct {
			Host              string        `toml:"host"`
			Port              int           `toml:"port"`
			ConnectionTimeout time.Duration `toml:"conn_timeout"`
			SoTimeout         time.Duration `toml:"read_timeout"`
			Password          string        `toml:"password"`
			Db                int           `toml:"db"`
		} `toml:"redis"`
		Postgres *struct {
			Host    string `toml:"host"`
			Port    int    `toml:"port"`
			DB      string `toml:"db"`
			User    string `toml:"user"`
			Pass    string `toml:"pass"`
			SSLMode string `toml:"ssl_mode"`
		} `toml:"postgres"`
	} `toml:"db"`
	CORS *struct {
		AllowAllOrigins        bool          `toml:"allow_all_origins"`
		AllowOrigins           []string      `toml:"allow_origins"`
		AllowMethods           []string      `toml:"allow_methods"`
		AllowHeaders           []string      `toml:"allow_headers"`
		AllowCredentials       bool          `toml:"allow_credentials"`
		ExposeHeaders          []string      `toml:"expose_headers"`
		MaxAge                 time.Duration `toml:"max_age"`
		AllowWildcard          bool          `toml:"allow_wildcard"`
		AllowBrowserExtensions bool          `toml:"allow_browser_extensions"`
		AllowWebSockets        bool          `toml:"allow_web_sockets"`
	} `toml:"cors"`
}

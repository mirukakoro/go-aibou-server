GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
O_NAME=mybinary

SECRET_KEY="this_is_not_secure_make_sure_to_set_it_to_a_random_string_before_building"

metadata:
	echo $(CI_COMMIT_SHA) > pkg/data/hash.txt
	echo $(CI_COMMIT_TAG) > pkg/data/version.txt
	echo $(CI_COMMIT_TIMESTAMP) > pkg/data/time.txt
	echo $(SECRET_KEY) > pkg/data/secret_key.txt
	echo $(REALM) > pkg/data/realm.txt

all: gen check test coverage build

gen:
	go generate

fmt: metadata
	go run utils/json/main.go --pattern=pkg/types/scenarios/*.json --in-place # format JSON
	go run utils/toml/main.go --pattern=*.toml --in-place # format TOML
	go fmt ./...

check: metadata
	go run utils/json/main.go --pattern=pkg/types/scenarios/*.json --check-only # check JSON
	go run utils/toml/main.go --pattern=*.toml --in-place # check TOML
	go vet ./...

test: metadata
	go test -race ./...

coverage:
	bash coverage.sh

coverage_upload: coverage
	bash $(curl -s https://codecov.io/bash)

build: metadata
	go build -race -ldflags "-extldflags '-static'" -o $(CI_PROJECT_DIR)/bin/server gitlab.com/colourdelete/go-aibou-server/cmd/server

build_release: metadata
	go build -ldflags "-extldflags '-static'" -o $(CI_PROJECT_DIR)/bin/server gitlab.com/colourdelete/go-aibou-server/cmd/server
